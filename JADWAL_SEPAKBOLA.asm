Org 100h
MOV AH,02H          ;nilai servis untuk mencetak karakter
MOV DL,'J'          ;Karakter ASCII yang akan dicetak
INT 21H             ;mencetak karakter
MOV DL,'A'
INT 21H
MOV DL,'D'
INT 21H
MOV DL,'W'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'L'
INT 21H
MOV DL,' '
INT 21H

MOV DL,'S'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'P'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'B'
INT 21H
MOV DL,'O'
INT 21H  
MOV DL,'L'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'A'



MOV DL,' '
INT 21H
MOV DL,'2'
INT 21H
MOV DL,'0'
INT 21H
MOV DL,'2'
INT 21H
MOV DL,'2'
INT 21H
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H
MOV DL,0DH
INT 21H

;KETERANGAN LIGA INGGRIS
MOV DL,'T'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,' '
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'('
INT 21H
MOV DL,'1'
INT 21H
MOV DL,')'
INT 21H
MOV DL,'='
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'R'
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'S'
INT 21H

MOV DL,' '
INT 21H
MOV DL,'1'
INT 21H
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H
;KETERANGAN LIGA SPANYOL
MOV DL,'T'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,' '
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'('
INT 21H
MOV DL,'2'
INT 21H
MOV DL,')'
INT 21H
MOV DL,'='
INT 21H
MOV DL,'S'
INT 21H
MOV DL,'P'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'Y'
INT 21H
MOV DL,'O'
INT 21H
MOV DL,'L'
INT 21H

MOV DL,' '
INT 21H
MOV DL,'2'
INT 21H
MOV DL,0DH      ;karakter enter
INT 21H         ;cetak karakter
MOV DL,0AH      ;karakter enter
INT 21H         ;cetak karakter
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H


jmp mulai
                                                                      
msg1: db 0dh,0ah," #HARI              LIGA INGGRIS                     WIB         STADION",0dh,0ah,0dh,0ah," *SENIN   :| CHELSEA VS LIVERPOOL     | 07.00-12.00 |  B.2.4  |",0dh,0ah,"  *SELASA :| LEEDS UNITED VS EVERTON             | 08.40-10.20 |  B.2.4  |",0dh,0ah,"  *SELASA :| MAN CITY VS MAN UNITED                  | 12.30-15.00 |  B.2.2  |",0dh,0ah,"  *RABU   :| ARSENAL VS LEICESTER CITY    | 07.00-08.40 |  B.2.4  |",0dh,0ah,"  *RABU   :| ASTON VILLA VS WEST HAM                  | 09.30-12.00 |  B.2.2  |",0dh,0ah,"  *JUMAT  :| NEWCASTLE VS SOUTHAMPTON             | 07.00-08.40 |  B.4.1  |",0dh,0ah,"  *JUMAT  :| ASTON VILLA VS DERBY COUNTY              | 07.00-08.40 |  D.2.J  |",0dh,0ah,'$'
msg2: db 0dh,0ah," #HARI              LIGA SPANYOL                     WIB         STADION",0dh,0ah,0dh,0ah," *SENIN   :| BARCELONA VS MADRID      | 07.00-12.00 |  B.2.4  |",0dh,0ah,"  *SELASA :| VILARREAL VS GRANADA                | 13.00-15.00 |  B.2.4  |",0dh,0ah,"  *SELASA :| ATLETICO MADRID VS ELCHE                | 09.30-12.00 |  B.2.2  |",0dh,0ah,"  *RABU   :| SEVILLA VS REAL BETIS        | 07.00-08.40 |  B.2.4  |",0dh,0ah,"  *RABU   :| REAL SOCIEDAD VS REAL VALLADOLID         | 12.30-03.00 |  B.2.2  |",0dh,0ah,"  *JUMAT  :| ATHLETIC BILBAO VS GETAFE            | 12.30-03.00 |  B.4.1  |",0dh,0ah,"  *JUMAT  :| CADIZ FC VS LEVANTE                      | 07.00-08.40 |  D.2.J  |",0dh,0ah,'$'

                                             
mulai:

mov ah, 01      ;nilai servis di ah dg 01  
int 21h         ;cetak karakter

cmp al, '1'     ; bandingkan al dengan 01
je kel_1        ; lompat ke kelompok_1   jika sama
    
cmp al, '2'     ;bandingkan al dengan 2
je kel_2        ; lompat ke kelompok_2   jika sama


jmp exit        ;lompat ke exit
exit:           ;exit program
ret

kel_1:
    mov dx, msg1    ;ambil offset msg1
    mov ah, 9       ;servis untuk cetak kalimat
    int 21h         ;cetak kalimat
    jmp mulai       ;lompat ke mulai
kel_2:
    mov dx, msg2    ;ambil offset msg1
    mov ah, 9       ;servis untuk cetak kalimat
    int 21h         ;cetak kalimat
    jmp mulai       ;lompat ke mulai
